package vn.mobifone.bigdata.util;

public class RespUtil {
    /**
     * Mainflux response
     * */
    public static final int MF_CREATE_SUCCESS_CODE = 201;
    public static final int MF_GET_SUCCESS_CODE = 200;
    
    public static final int MF_DELETE_SUCCESS_CODE = 204;
    /**
     * Response status
     * */
    public static final int RESPONSE_CODE_OK = 1;
    public static final int RESPONSE_CODE_NOK = 0;
    public static final String EXCEPTION_CODE = "SYSTEM_ERROR";
    public static final String EXCEPTION_MESSAGE = "Exception";
    /**
     * Version
     * */
    public static final String VERSION_NOT_FOUND = "Version not found";
    public static final String VERSION_NOT_CODE = "VERSION_NOT_FOUND";
    /**
     * Mainflux auth
     * */
    public static final String MAINFLUX_LOGIN_CODE = "AUTH_ERROR";
    public static final String MAINFLUX_LOGIN_MESSAGE = "Không lấy được Token truy cập hệ thống";
    /**
     * Verify MCU
     * */
    public static final String THING_VERIFY_SUCCESS_CODE = "MCU_VERIFY_SUCCESS";
    public static final String THING_VERIFY_SUCCESS_MESSAGE = "Verify MCU thành công";
    
    public static final String THING_MAPPED_CODE = "MCU_VERIFY_ERROR_0001";
    public static final String THING_MAPPED_MESSAGE = "MCU đang tồn tại";

    public static final String TTTM_CREATE_THING_CODE_02 = "MCU_VERIFY_ERROR_0002";
    public static final String TTTM_CREATE_THING_MESSAGE_02 = "Không MAP được MCU-Things trên DB";

    public static final String MAINFLUX_GET_THING_CODE = "MCU_VERIFY_ERROR_0003";
    public static final String MAINFLUX_GET_THING_MESSAGE = "Không lấy được thông tin ThingId";

    public static final String MAINFLUX_CREATE_THING_CODE = "MCU_VERIFY_ERROR_0004";
    public static final String MAINFLUX_CREATE_THING_MESSAGE = "Không tạo được Things trên DB";





   
    
    
    /**
     * MCU Delete
     * */
    public static final String MCU_DELETE_SUCCESS_CODE = "MCU_DELETE_SUCCESS";
    public static final String MCU_DELETE_SUCCESS_MESSAGE = "Xoá MCU thành công";
    
    public static final String THING_MAPPED_NOT_FOUND_CODE = "MCU_DELETE_ERROR_0001";
    public static final String THING_MAPPED_NOT_FOUND_MESSAGE = "MCU chưa tồn tại trên hệ thống";
    
    public static final String MAINFLUX_THING_CHANNEL_CODE = "MCU_DELETE_ERROR_0002";
    public static final String MAINFLUX_THING_CHANNEL_MESSAGE = "Lỗi khi xoá quyền truy cập Channel hoặc Things không có trên Channel";
    
    public static final String THING_UPDATE_MAPPER_CODE = "MCU_DELETE_ERROR_0003";
    public static final String THING_UPDATE_MAPPER_MESSAGE = "Không thể cập nhật trạng thái bảng Map";
    
    /**
     * MCU collect logs
     * */

    public static final String MCU_COLLECT_LOG_CODE_SUCCESS = "COL_LOG_SUCCESS";
    public static final String MCU_COLLECT_LOG_MESSAGE_SUCCESS = "Ghi log thành công";

    public static final String MCU_COLLECT_LOG_FAILED_CODE = "COL_LOG_ERROR_0001";
    public static final String MCU_COLLECT_LOG_FAILED_MESSAGE = "Ghi log thất bại";

    /**
     * Database
     * */
    public static final String DATABASE_STATUS_SUCCESS = "1";

    
    /**
     * Collect Logs
     * */
    
    /**
     * Set Group
     * */
    public static final int MF_GRANT_PERMISSION_SUCCESS_CODE = 200;
    public static final String GRANT_PERMISSION_SUCCESS_CODE = "SET_GROUP_SUCCESS";
    public static final String GRANT_PERMISSION_SUCCESS_MESSAGE = "Set group thành công";
    
    public static final String GRANT_PERMISSION_ERROR_CODE = "SET_GROUP_ERROR_0001";
    public static final String GRANT_PERMISSION_ERROR_MESSAGE = "Set group không thành công";
    
    public static final String GRANT_PERMISSION_NOT_FOUND_CODE = "SET_GROUP_ERROR_0002";
    public static final String GRANT_PERMISSION_NOT_FOUND_MESSAGE = "MCU chưa tồn tại trên hệ thống";
    
    public static final String MCU_ID_NULL_CODE = "MCU_ID_IS_NULL";
    public static final String MCU_ID_NULL_MESSAGE = "MCUID không được bỏ trống";
}
