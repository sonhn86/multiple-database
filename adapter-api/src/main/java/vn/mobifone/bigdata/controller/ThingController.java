package vn.mobifone.bigdata.controller;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.mobifone.bigdata.controller.services.ThingService;
import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;
import vn.mobifone.bigdata.env.AppConfig;
import vn.mobifone.bigdata.env.MainfluxGwConfig;
import vn.mobifone.bigdata.repository.tttm.tttmrepo.McuRepositoryCustom;
import vn.mobifone.bigdata.service.MainfluxGwService;
import vn.mobifone.bigdata.service.RedisService;

@RestController
@RequestMapping("/api/{version}/tttm/*")
public class ThingController extends BaseController {

	private static final Logger logger = LogManager.getLogger(ThingController.class);

	@Autowired
	AppConfig appConfig;

	@Autowired
	RedisService redisService;

	@Autowired
	MainfluxGwService mainfluxGwService;

	@Autowired
	McuRepositoryCustom mcuRepositoryCustom;

	@Autowired
	MainfluxGwConfig mainfluxGwConfig;

	@RequestMapping(value = "/verify-mcu", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ApiResponse verifyMcu(@PathVariable("version") String version, @RequestBody TTTMThing tttmThing) {
		return this.baseMethod(new Callable<ApiResponse>() {
			@Override
			public ApiResponse call() throws Exception {
				Method thingServiceInstanceMethod = ThingService.class.getMethod("verifyMcu" + version.toUpperCase(),
						TTTMThing.class);
				ThingService thingServiceInstance = new ThingService(redisService, mainfluxGwService,
						mcuRepositoryCustom, mainfluxGwConfig);

				/**
				 * code example version 2
				 */
//            Method thingServiceInstanceMethod
//                    = ThingServiceV2.class.getMethod("verifyMcu"+version.toUpperCase(), TTTMThing.class);
//            ThingServiceV2 thingServiceInstance = new ThingServiceV2(redisService, mainfluxGwService, mcuRepositoryCustom);
				/**
				 * */

				return (ApiResponse) thingServiceInstanceMethod.invoke(thingServiceInstance, tttmThing);
			}
		});
	}

	@RequestMapping(value = "/delete-mcu", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ApiResponse deleteMcu(@PathVariable("version") String version, @RequestBody TTTMThing tttmThing) {
		return this.baseMethod(new Callable<ApiResponse>() {
			@Override
			public ApiResponse call() throws Exception {
				Method thingServiceInstanceMethod = ThingService.class.getMethod("deleteMcu" + version.toUpperCase(),
						TTTMThing.class);
				ThingService thingServiceInstance = new ThingService(redisService, mainfluxGwService,
						mcuRepositoryCustom, mainfluxGwConfig);
				return (ApiResponse) thingServiceInstanceMethod.invoke(thingServiceInstance, tttmThing);
			}
		});
	}

	@RequestMapping(value = "/set-group", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ApiResponse setGroupMcu(@PathVariable("version") String version, @RequestBody TTTMThing tttmThing) {
		return this.baseMethod(new Callable<ApiResponse>() {
			@Override
			public ApiResponse call() throws Exception {
				Method thingServiceInstanceMethod = ThingService.class.getMethod("setGroupMcu" + version.toUpperCase(),
						TTTMThing.class);
				ThingService thingServiceInstance = new ThingService(redisService, mainfluxGwService,
						mcuRepositoryCustom, mainfluxGwConfig);
				return (ApiResponse) thingServiceInstanceMethod.invoke(thingServiceInstance, tttmThing);
			}
		});
	}
	
	@RequestMapping(value = "/collect-log", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ApiResponse collectLog(@PathVariable("version") String version, @RequestBody TTTMThing tttmThing) {
		return this.baseMethod(new Callable<ApiResponse>() {
			@Override
			public ApiResponse call() throws Exception {
				Method thingServiceInstanceMethod = ThingService.class.getMethod("collectLog" + version.toUpperCase(),
						TTTMThing.class);
				ThingService thingServiceInstance = new ThingService(redisService, mainfluxGwService,
						mcuRepositoryCustom, mainfluxGwConfig);
				return (ApiResponse) thingServiceInstanceMethod.invoke(thingServiceInstance, tttmThing);
			}
		});
	}
}
