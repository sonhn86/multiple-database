package vn.mobifone.bigdata.controller.services;

import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;
import vn.mobifone.bigdata.env.MainfluxGwConfig;
import vn.mobifone.bigdata.repository.tttm.tttmrepo.McuRepositoryCustom;
import vn.mobifone.bigdata.service.MainfluxGwService;
import vn.mobifone.bigdata.service.RedisService;
import vn.mobifone.bigdata.util.RespUtil;

public class ThingServiceV2 extends ThingService{

    public ThingServiceV2(RedisService redisService, MainfluxGwService mainfluxGwService, McuRepositoryCustom mcuRepositoryCustom, MainfluxGwConfig mainfluxGwConfig) {
        super(redisService, mainfluxGwService, mcuRepositoryCustom, mainfluxGwConfig);
    }

    public ApiResponse verifyMcuV2(TTTMThing tttmThing){
        ApiResponse apiResponse = new ApiResponse();
        try {
            apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
            apiResponse.setCode(RespUtil.THING_MAPPED_CODE);
            apiResponse.setMessage(RespUtil.THING_MAPPED_MESSAGE);
        }catch (Exception e){
        }
        return apiResponse;
    }
}
