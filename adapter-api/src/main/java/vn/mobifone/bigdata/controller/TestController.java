package vn.mobifone.bigdata.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vn.mobifone.bigdata.controller.services.ThingService;
import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;
import vn.mobifone.bigdata.env.AppConfig;
import vn.mobifone.bigdata.env.MainfluxGwConfig;
import vn.mobifone.bigdata.repository.tttm.tttmrepo.McuRepositoryCustom;
import vn.mobifone.bigdata.service.MainfluxGwService;
import vn.mobifone.bigdata.service.RedisService;
import vn.mobifone.bigdata.service.TestService;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api/{version}/test/*")
public class TestController extends BaseController {

	private static final Logger logger = LogManager.getLogger(TestController.class);

	@Autowired
	TestService testService;

	@RequestMapping(value = "/get-infor", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getInfor() throws Exception {
		return testService.testConnect();
	}
}
