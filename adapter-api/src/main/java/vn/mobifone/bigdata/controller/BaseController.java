package vn.mobifone.bigdata.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.util.RespUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;

public class BaseController{

    private static final Logger logger = LogManager.getLogger(BaseController.class);

    protected ApiResponse baseMethod(Callable<ApiResponse> func){
        ApiResponse apiResponse = new ApiResponse();
        try {
            return func.call();
        }catch ( NoSuchMethodException e){
            logger.error(e);
            apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
            apiResponse.setCode(RespUtil.VERSION_NOT_CODE);
            apiResponse.setMessage(RespUtil.VERSION_NOT_FOUND);
        } catch (IllegalAccessException e) {
            logger.error(e);
            apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
            apiResponse.setCode(RespUtil.VERSION_NOT_CODE);
            apiResponse.setMessage(RespUtil.VERSION_NOT_FOUND);
        } catch (InvocationTargetException e) {
            logger.error(e);
            apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
            apiResponse.setCode(RespUtil.VERSION_NOT_CODE);
            apiResponse.setMessage(RespUtil.VERSION_NOT_FOUND);
        } catch (Exception e){
            logger.error(e);
            apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
            apiResponse.setCode(RespUtil.EXCEPTION_CODE);
            apiResponse.setMessage(RespUtil.EXCEPTION_MESSAGE);
        }
        return apiResponse;
    }

}
