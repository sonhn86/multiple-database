package vn.mobifone.bigdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"vn.mobifone.bigdata.*"})
public class AdapterApiApplication {

	public static void main(String[] args) {
//		SpringApplication.run(AdapterApiApplication.class, args);
		try {
			SpringApplication.run(AdapterApiApplication.class, args);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
