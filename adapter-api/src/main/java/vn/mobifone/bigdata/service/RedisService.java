package vn.mobifone.bigdata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisService {

    @Autowired
    private RedisTemplate<String, String> template;

    public Object getValue(final String key) {
        return template.opsForValue().get(key);
    }

    public Object getValue(final String key, final String subKey){
        return template.opsForHash().get(key,subKey);
    }

    public void setValue(final String key, final String value) {
        template.opsForValue().set(key, value);
    }

    public void setValue(final String key, final String value, int timeout) {
        template.opsForValue().set(key, value, timeout, TimeUnit.DAYS);
    }

    public void setHValue(final String key, final String subKey, final String value) {
        template.opsForHash().put(key, subKey, value);
    }

    public void setValue(final String key, final String value, int timeout, TimeUnit timeUnit) {
        template.opsForValue().set(key, value, timeout, timeUnit);
    }

    public void removeKey(final String key) {
        template.delete(key);
    }

    public void removeKeyForHash(final String key){
        template.opsForHash().delete(key);
    }

    public boolean haskey(final String key) {
        return template.hasKey(key);
    }
}
