package vn.mobifone.bigdata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import vn.mobifone.bigdata.repository.things.thingsrepo.ThingsRepositoryCustom;
import vn.mobifone.bigdata.repository.users.usersrepo.UsersRepositoryCustom;

import java.util.concurrent.TimeUnit;

@Service
public class TestService {

    @Autowired
    UsersRepositoryCustom usersRepositoryCustom;

    @Autowired
    ThingsRepositoryCustom thingsRepositoryCustom;

    public String testConnect() throws Exception {
        String email = usersRepositoryCustom.getAll().get(0).getEmail();
        String name = thingsRepositoryCustom.getAll().get(0).getName();
        return email+" "+ name;
    }
}
