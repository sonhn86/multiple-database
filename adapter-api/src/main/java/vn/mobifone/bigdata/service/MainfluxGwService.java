package vn.mobifone.bigdata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.mobifone.bigdata.en.HttpMethod;
import vn.mobifone.bigdata.entity.respone.mainflux.MainfluxResponse;
import vn.mobifone.bigdata.entity.things.ThingInfo;
import vn.mobifone.bigdata.env.MainfluxGwConfig;
import vn.mobifone.bigdata.ws.mainflux.MainfluxServiceImp;

import java.util.HashMap;
import java.util.Map;

@Service
public class MainfluxGwService {

    @Autowired
    MainfluxGwConfig mainfluxGwConfig;

    /**
     * login - tao token
     * */

    public MainfluxResponse token(){
        try {
            Map<String, String> headers = new HashMap<>();
            String jsonBody = "{ \"email\":\"" + mainfluxGwConfig.getSysUser() + "\" , \"password\":\""+mainfluxGwConfig.getSysPassword()+"\"}";
            String endpoint = mainfluxGwConfig.getUserEndpoint() + mainfluxGwConfig.getGetUserToken();
            MainfluxServiceImp mainfluxServiceImp = new MainfluxServiceImp(endpoint, jsonBody, headers);
            MainfluxResponse m = mainfluxServiceImp.executeSevice();
            return m;
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * tao thing tren nen tang main-flux
     * */

    public MainfluxResponse createThing(String key, String name, Object metadata, String token){
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);
            String jsonBody = "{ \"key\":\"" + key + "\" , \"name\":\""+name+"\", \"metadata\":"+"{}"+"}";
            String endpoint = mainfluxGwConfig.getThingEndpoint() + mainfluxGwConfig.getCreateThing();
            MainfluxServiceImp mainfluxServiceImp = new MainfluxServiceImp(endpoint, jsonBody, headers, HttpMethod.POST);
            MainfluxResponse m = mainfluxServiceImp.executeSevice();
            return m;
        }catch (Exception e){
            throw e;
        }
    }

    /**
     * lay thing id tren nen tang main-flux
     * */

    public MainfluxResponse getThingId(String key, String token){
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);
            String jsonBody = "{ \"token\":\"" + key + "\" }";
            String endpoint = mainfluxGwConfig.getThingIdEndpoint() + mainfluxGwConfig.getGetThingId();
            MainfluxServiceImp mainfluxServiceImp = new MainfluxServiceImp(endpoint, jsonBody, headers, HttpMethod.POST);
            MainfluxResponse m = mainfluxServiceImp.executeSevice();
            return m;
        }catch (Exception e){
            throw e;
        }
    }
    
    /**
     * lay thing id tren nen tang main-flux qua Name và Key
     * */

    public String getThingIdByNameAndKey(String name, String key, String token){
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);
            String jsonBody = "";
            String endpoint = mainfluxGwConfig.getThingEndpoint() + mainfluxGwConfig.getCreateThing() + "?limit=1000&name="+name;
            MainfluxServiceImp mainfluxServiceImp = new MainfluxServiceImp(endpoint, jsonBody, headers, HttpMethod.GET);
            MainfluxResponse m = mainfluxServiceImp.executeSevice();
            
            if(m != null) {
            	for(ThingInfo t : m.getThings()) {
            		if(t.getName().equals(name) && t.getKey().equals(key))
            		{
            			return t.getId();
            		}
            	}
            	return "";
            }
            else
            {
            	return "";
            }
            
        }catch (Exception e){
        	e.printStackTrace();
            return "";
        }
    }

    /**
     * xoa thing tren nen tang mainflux
     * */

    public MainfluxResponse delete(String id, String token){
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);
            String jsonBody = "{}";
            String endpoint = mainfluxGwConfig.getThingEndpoint() + mainfluxGwConfig.getRemoveThing()+"/"+id;
            MainfluxServiceImp mainfluxServiceImp = new MainfluxServiceImp(endpoint, jsonBody, headers, HttpMethod.DELETE);
            MainfluxResponse m = mainfluxServiceImp.executeSevice();
            return m;
        }catch (Exception e){
            throw e;
        }
    }

    /**
     * xoa quyen truy cap cua thing vao channel tren nen tang mainflux
     * */

    public MainfluxResponse deleteThingChannelPermission(String token, String id, String channelId){
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);
            String jsonBody = "{}";
            String endpoint = mainfluxGwConfig.getThingEndpoint() + mainfluxGwConfig.getChannel()+"/"+channelId+"/"+mainfluxGwConfig.getCreateThing()+"/"+id;
            MainfluxServiceImp mainfluxServiceImp = new MainfluxServiceImp(endpoint, jsonBody, headers, HttpMethod.DELETE);
            MainfluxResponse m = mainfluxServiceImp.executeSevice();
            return m;
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }
    
    /**
     * thêm quyen truy cap cua thing vao channel tren nen tang mainflux
     * */

    public MainfluxResponse grantThingChannelPermission(String token, String id, String channelId){
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);
            String jsonBody = "{}";
            String endpoint = mainfluxGwConfig.getThingEndpoint() + mainfluxGwConfig.getChannel()+"/"+channelId+"/"+mainfluxGwConfig.getCreateThing()+"/"+id;
            MainfluxServiceImp mainfluxServiceImp = new MainfluxServiceImp(endpoint, jsonBody, headers, HttpMethod.PUT);
            MainfluxResponse m = mainfluxServiceImp.executeSevice();
            return m;
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

}
