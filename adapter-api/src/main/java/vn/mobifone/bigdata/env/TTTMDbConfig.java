package vn.mobifone.bigdata.env;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "tttmEntityManagerFactory", basePackages = {"vn.mobifone.bigdata.repositoryImp.tttm" })
public class TTTMDbConfig {


	@Bean(name = "tttmDataSourceProperties")
	@ConfigurationProperties("tttm.datasource")
	public DataSourceProperties dataSourceProperties() {
		return new DataSourceProperties();
	}


	@Bean(name = "tttmDataSource")
	@ConfigurationProperties("tttm.datasource.configuration")
	public DataSource dataSource(@Qualifier("tttmDataSourceProperties") DataSourceProperties tttmDataSourceProperties) {
		return tttmDataSourceProperties.initializeDataSourceBuilder()
				.build();
	}
	

	@Bean(name = "tttmEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("tttmDataSource") DataSource tttmDataSource) {
		return builder.dataSource(tttmDataSource).packages("vn.mobifone.bigdata.repository.tttm.tttmmodel").build();
	}
	

	@Bean(name = "tttmTransactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("tttmEntityManagerFactory") EntityManagerFactory tttmEntityManagerFactory) {
		return new JpaTransactionManager(tttmEntityManagerFactory);
	}
}