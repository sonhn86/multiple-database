package vn.mobifone.bigdata.env;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "usersEntityManagerFactory", basePackages = {"vn.mobifone.bigdata.repositoryImp.users" })
public class UsersDbConfig {

	@Primary
	@Bean(name = "usersDataSourceProperties")
	@ConfigurationProperties("users.datasource")
	public DataSourceProperties dataSourceProperties() {
		return new DataSourceProperties();
	}

	@Primary
	@Bean(name = "usersDataSource")
	@ConfigurationProperties("users.datasource.configuration")
	public DataSource dataSource(@Qualifier("usersDataSourceProperties") DataSourceProperties usersDataSourceProperties) {
		return usersDataSourceProperties.initializeDataSourceBuilder()
				.type(HikariDataSource.class)
				.build();
	}
	
	@Primary
	@Bean(name = "usersEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("usersDataSource") DataSource usersDataSource) {
		return builder.dataSource(usersDataSource).packages("vn.mobifone.bigdata.repository.users.usersmodel").build();
	}
	
	@Primary
	@Bean(name = "usersTransactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("usersEntityManagerFactory") EntityManagerFactory usersEntityManagerFactory) {
		return new JpaTransactionManager(usersEntityManagerFactory);
	}
}