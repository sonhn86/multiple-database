package vn.mobifone.bigdata.entity.things;
/**
 * Default mainflux thing
 * */
import com.fasterxml.jackson.core.JsonProcessingException;

public class MainfluxThing {

    private String thingId;
    private String thingKey;
    private String thingName;
    private String thingsOwner;
    private Object thingMetadata;

    public String getThingId() {
        return thingId;
    }

    public void setThingId(String thingId) {
        this.thingId = thingId;
    }

    public String getThingKey() {
        return thingKey;
    }

    public void setThingKey(String thingKey) {
        this.thingKey = thingKey;
    }

    public String getThingName() {
        return thingName;
    }

    public void setThingName(String thingName) {
        this.thingName = thingName;
    }

    public String getThingsOwner() {
        return thingsOwner;
    }

    public void setThingsOwner(String thingsOwner) {
        this.thingsOwner = thingsOwner;
    }

    public Object getThingMetadata() {
        return thingMetadata;
    }

    public void setThingMetadata(Object thingMetadata) {
        this.thingMetadata = thingMetadata;
    }

    @Override
    public String toString() {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
