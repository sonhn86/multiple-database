package vn.mobifone.bigdata.entity.respone;

import com.fasterxml.jackson.core.JsonProcessingException;

public class DBResponse {

    private String errorId;
    private String errorCode;
    private String errorDesc;

    public DBResponse(String errorId, String errorCode, String errorDesc) {
        this.errorId = errorId;
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public DBResponse() {
    }

    public String getErrorId() {
        return errorId;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    @Override
    public String toString() {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
