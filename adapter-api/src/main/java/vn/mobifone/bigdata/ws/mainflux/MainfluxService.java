package vn.mobifone.bigdata.ws.mainflux;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import vn.mobifone.bigdata.en.HttpMethod;
import vn.mobifone.bigdata.entity.respone.mainflux.MainfluxResponse;
import vn.mobifone.bigdata.ws.base.OkHttpService;

import java.io.IOException;
import java.util.Map;

public class MainfluxService  extends OkHttpService<MainfluxResponse> {

    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public MainfluxService(String url, String jsonBody, Map<String, String> headerParams) {
        this.url = url;
        this.jsonBody = jsonBody;
        this.headerParams = headerParams;
        this.httpMethod = HttpMethod.POST;
    }

    public MainfluxService(String url, String jsonBody, Map<String, String> headerParams, HttpMethod httpMethod) {
        this.url = url;
        this.jsonBody = jsonBody;
        this.headerParams = headerParams;
        this.httpMethod = httpMethod;
    }

    @Override
    public MainfluxResponse execute() throws IOException {
        RequestBody body = RequestBody.create(jsonBody, mediaType);
        Headers headers = Headers.of(headerParams);
        Request request = null;
        switch (this.httpMethod){
            case PUT:
                request = new Request.Builder()
                    .url(url)
                    .put(body)
                    .headers(headers)
                    .build();
                break;
            case POST:
                request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .headers(headers)
                    .build();
                break;
            case DELETE:
                request = new Request.Builder()
                    .url(url)
                    .delete(body)
                    .headers(headers)
                    .build();
                break;
            case GET:
                request = new Request.Builder()
                    .url(url)
                    .get()
                    .headers(headers)
                    .build();
                break;
            case UNKNOWN:
                break;
        }
        try (Response response = client.newCall(request).execute()) {
            String resp = response.body().string();
            ObjectMapper objectMapper = new ObjectMapper();
            MainfluxResponse mainfluxResponse = null;
            if(resp != null && resp != ""){
                mainfluxResponse = (MainfluxResponse) objectMapper.readValue(resp, MainfluxResponse.class);
            }else{
                mainfluxResponse = new MainfluxResponse();
            }
            mainfluxResponse.setCode(response.code());
            return mainfluxResponse;
        }
    }

    @Override
    public void init() {
        this.client = new OkHttpClient();
        this.mediaType = JSON;
    }

    @Override
    public String toString() {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
