package vn.mobifone.bigdata.ws.base;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;

public class WSLogger <T extends OkHttpService> implements WSHandle<T>{
    private static final Logger logger = LogManager.getLogger(WSLogger.class);

    private Long startTime;
    private Long endTime;

    public WSLogger() {
    }

    @Override
    public void afterInitCommand(T ser) {
        startTime = new Date().getTime();
    }

    @Override
    public void afterExecute(T ser) {
        endTime = new Date().getTime();
//        DbUtil.insertMhttGWLog(tranid, isdn, ser, ser.getEx() == null ? "SUCCESS" : "FAILURE", (endTime == null || startTime == null) ? null : endTime - startTime);
        logger.info("--ser: "+ser.toString()+" --result: "+(ser.getEx() == null ? "SUCCESS" : "FAILURE")+" --time: "+((endTime == null || startTime == null) ? null : endTime - startTime));
    }

    @Override
    public void afterException(T ser) {
        endTime = new Date().getTime();
//        DbUtil.insertMhttGWLog(tranid, isdn, ser, ser.getEx() == null ? "SUCCESS" : "FAILURE", (endTime == null || startTime == null) ? null : endTime - startTime);
        logger.error("--ser: "+ser.toString()+" --result: "+(ser.getEx() == null ? "SUCCESS" : "FAILURE")+" --time: "+((endTime == null || startTime == null) ? null : endTime - startTime));
    }
}
