package vn.mobifone.bigdata.exception;

import vn.mobifone.bigdata.en.AuthExceptionType;
import vn.mobifone.bigdata.en.McuExceptionType;
import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.util.RespUtil;

public class AuthException extends Exception implements ApiException{

    private AuthExceptionType authExceptionType;

    public AuthException() {
        super();
    }

    public AuthException(String message) {
        super(message);
    }

    public AuthException(Throwable throwable) {
        super(throwable);
    }

    public AuthException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public AuthException(AuthExceptionType type){
        super(type.getValue());
        authExceptionType = type;
    }

    public ApiResponse getMessageFromType(){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
        switch (authExceptionType){
            case GET_TOKEN_ERROR:
                apiResponse.setCode(RespUtil.MAINFLUX_LOGIN_CODE);
                apiResponse.setMessage(RespUtil.MAINFLUX_LOGIN_MESSAGE);
                break;
            case UNKNOWN:
                apiResponse.setCode(RespUtil.EXCEPTION_CODE);
                apiResponse.setMessage(RespUtil.EXCEPTION_MESSAGE);
                break;
        }
        return apiResponse;
    }
}
