package vn.mobifone.bigdata.exception;

import vn.mobifone.bigdata.en.AuthExceptionType;
import vn.mobifone.bigdata.en.ThingExceptionType;
import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.util.RespUtil;

public class ThingException extends Exception implements ApiException{

    private ThingExceptionType thingExceptionType;

    public ThingException() {
        super();
    }

    public ThingException(String message) {
        super(message);
    }

    public ThingException(Throwable throwable) {
        super(throwable);
    }

    public ThingException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ThingException(ThingExceptionType type){
        super(type.getValue());
        thingExceptionType = type;
    }

    public ApiResponse getMessageFromType(){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
        switch (thingExceptionType){
            case NO_THING_INFO:
                apiResponse.setCode(RespUtil.MAINFLUX_GET_THING_CODE);
                apiResponse.setMessage(RespUtil.MAINFLUX_GET_THING_MESSAGE);
                break;
            case CANNOT_CREATE_THING:
                apiResponse.setCode(RespUtil.MAINFLUX_CREATE_THING_CODE);
                apiResponse.setMessage(RespUtil.MAINFLUX_CREATE_THING_MESSAGE);
                break;
            case CANNOT_UPDATE_THING_STATUS:
                apiResponse.setCode(RespUtil.THING_UPDATE_MAPPER_CODE);
                apiResponse.setMessage(RespUtil.THING_UPDATE_MAPPER_MESSAGE);
                break;
            case CANNOT_MAP_MCU_THING:
                apiResponse.setCode(RespUtil.TTTM_CREATE_THING_CODE_02);
                apiResponse.setMessage(RespUtil.TTTM_CREATE_THING_MESSAGE_02);
                break;
            case UNKNOWN:
                apiResponse.setCode(RespUtil.EXCEPTION_CODE);
                apiResponse.setMessage(RespUtil.EXCEPTION_MESSAGE);
                break;
        }
        return apiResponse;
    }
}
