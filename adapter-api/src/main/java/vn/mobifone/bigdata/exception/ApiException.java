package vn.mobifone.bigdata.exception;

import vn.mobifone.bigdata.entity.respone.ApiResponse;

public interface ApiException {
    public ApiResponse getMessageFromType();
}
