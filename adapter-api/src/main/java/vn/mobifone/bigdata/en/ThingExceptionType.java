package vn.mobifone.bigdata.en;

import java.util.HashMap;
import java.util.Map;

public enum ThingExceptionType {

    UNKNOWN("UNKNOW"), NO_THING_INFO("NO_THING_INFO"), CANNOT_CREATE_THING("CANNOT_CREATE_THING"),
    CANNOT_MAP_MCU_THING("CANNOT_MAP_MCU_THING"), CANNOT_UPDATE_THING_STATUS("CANNOT_UPDATE_THING_STATUS");

    private String value;

    private ThingExceptionType(String t) {
        value = t;
    }

    public String getValue() {
        return value;
    }

    private static final Map<String, ThingExceptionType> stringToTypeMap = new HashMap<>();

    static {
        for (ThingExceptionType item : ThingExceptionType.values()) {
            stringToTypeMap.put(item.getValue(), item);
        }
    }

    public static ThingExceptionType fromString(int i) {

        ThingExceptionType type = stringToTypeMap.get(i);
        if (type == null)
            return ThingExceptionType.UNKNOWN;
        return type;
    }

}
