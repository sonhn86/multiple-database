package vn.mobifone.bigdata.en;

import java.util.HashMap;
import java.util.Map;

public enum McuExceptionType {

    UNKNOWN("UNKNOW"), MCU_NOT_FOUND("MCU_NOT_FOUND")
    , MCU_EXITED("MCU_EXITED"), CANNOT_SET_GROUP("CANNOT_SET_GROUP"), MCU_NOT_EXIT("MCU_NOT_EXIT"), CANNOT_DELETE_PERMISSION_CHANNEL("CANNOT_DELETE_PERMISSION_CHANNEL"), MCU_ID_NULL("MCU_ID_NULL");

    private String value;

    private McuExceptionType(String t) {
        value = t;
    }

    public String getValue() {
        return value;
    }

    private static final Map<String, McuExceptionType> stringToTypeMap = new HashMap<>();

    static {
        for (McuExceptionType item : McuExceptionType.values()) {
            stringToTypeMap.put(item.getValue(), item);
        }
    }

    public static McuExceptionType fromString(int i) {

        McuExceptionType type = stringToTypeMap.get(i);
        if (type == null)
            return McuExceptionType.UNKNOWN;
        return type;
    }
}
