package vn.mobifone.bigdata.en;

import java.util.HashMap;
import java.util.Map;

public enum HttpMethod {

    UNKNOWN("UNKNOW"), POST("POST"), PUT("PUT"), DELETE("DELETE"), GET("GET");

    private String value;

    private HttpMethod(String t) {
        value = t;
    }

    public String getValue() {
        return value;
    }

    private static final Map<String, HttpMethod> stringToTypeMap = new HashMap<>();

    static {
        for (HttpMethod item : HttpMethod.values()) {
            stringToTypeMap.put(item.getValue(), item);
        }
    }

    public static HttpMethod fromString(int i) {

        HttpMethod type = stringToTypeMap.get(i);
        if (type == null)
            return HttpMethod.UNKNOWN;
        return type;
    }
}
