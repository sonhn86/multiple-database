package vn.mobifone.bigdata.repositoryImp.things;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.respone.DBResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;
import vn.mobifone.bigdata.repository.things.thingsmodel.Things;
import vn.mobifone.bigdata.repository.things.thingsrepo.ThingsRepositoryCustom;
import vn.mobifone.bigdata.repository.users.usersrepo.UsersRepositoryCustom;
import vn.mobifone.bigdata.util.RespUtil;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class ThingsRepositoryCustomImp implements ThingsRepositoryCustom {

    private static final Logger logger = LoggerFactory.getLogger(ThingsRepositoryCustomImp.class);

    @Autowired
    @Qualifier("thingsDataSource")
    DataSource dataSource;

    @Override
    public List<Things> getAll() throws Exception {
        List<Things> thingsList = new ArrayList<>();
        // TODO Auto-generated method stub
        ApiResponse apiResponse = new ApiResponse();
        String sqlCall ="select * from things";
        try(Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sqlCall);
        ){
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                 Long id = rs.getLong(1);
                 String owner = rs.getString(2);
                 String key = rs.getString(3);
                 String name = rs.getString(4);
                 String  metadata = rs.getString(5);
                thingsList.add(new Things(id,owner,key,name,metadata));
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            throw e;
        }
        return thingsList;
    }

}
