package vn.mobifone.bigdata.repositoryImp.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.respone.DBResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;
import vn.mobifone.bigdata.repository.users.usersmodel.User;
import vn.mobifone.bigdata.repository.users.usersrepo.UsersRepositoryCustom;
import vn.mobifone.bigdata.util.RespUtil;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class UsersRepositoryCustomImp implements UsersRepositoryCustom {

    private static final Logger logger = LoggerFactory.getLogger(UsersRepositoryCustomImp.class);

    @Autowired
    @Qualifier("usersDataSource")
    DataSource dataSource;



    @Override
    public List<User> getAll() throws Exception {
        List<User> userList = new ArrayList<>();
        // TODO Auto-generated method stub
        ApiResponse apiResponse = new ApiResponse();
        String sqlCall ="select * from users";
        try(Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sqlCall);
        ){
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Long id = rs.getLong(1);
                String email = rs.getString(2);
                String password = rs.getString(3);
                String metadata = rs.getString(4);
                userList.add( new User(id,email,password,metadata));
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            throw e;
        }
        return userList;
    }
}
