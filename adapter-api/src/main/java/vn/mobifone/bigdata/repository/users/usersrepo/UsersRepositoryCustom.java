package vn.mobifone.bigdata.repository.users.usersrepo;

import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.respone.DBResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;
import vn.mobifone.bigdata.repository.users.usersmodel.User;

import java.util.List;

public interface UsersRepositoryCustom {
    public List<User> getAll() throws Exception;
}
